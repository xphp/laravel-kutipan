@extends('layouts.app')

@section('content')

<div class="container">
  <div class="jumbotron">
    <h1 class="display-4">{{ $quote->title }}</h1>
    <h5>Ditulis oleh: {{ $quote->user->name }}</h5>
    <p>{{ $quote->subject }}</p>
    
    @if ($quote->isOwner())
    <button class="btn btn-warning" type="submit">Edit</button>
    <button class="btn btn-danger" type="submit">Hapus</button>
    @endif
    <br><br>
    <p><a href="/quotes" class="btn btn-primary btn-lg" >Balik ke Daftar</a></p>

  </div>
</div>
@endsection
