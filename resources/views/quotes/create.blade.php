@extends('layouts.app')

@section('content')
<div class="container">
  @if (count($errors) > 0)
    <div class="alert alert-danger" role="alert">
      @foreach ($errors->all() as $error)
          <ul>
            <li> {{ $error }} </li>
          </ul>
      @endforeach
    </div>
  @endif
  <div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">Kutipan</div>
            <div class="card-body">
              <form action="/quotes" method="post">
                @csrf
                <div class="form-group">
                  <label for="title">Judul</label>
                  <input id="title" class="form-control" type="text" name="title" value=" {{ old('title') }} " placeholder="tulis judul di sini...">
                </div>
                <div class="form-group">
                  <label for="subject">Isi Kutipan</label>
                  <textarea id="subject" class="form-control" name="subject" rows="3"> {{ old('subject') }} </textarea>
                </div>
                <button class="btn btn-primary btn-block" type="submit">Kirim</button>
              </form>
            </div>
        </div>
    </div>
  </div>
</div>
@endsection
